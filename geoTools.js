const turf = require('@turf/turf');

function deduplicateLine(line) {
    if (!Array.isArray(line)) throw ("geoTools.deduplicateLine line is not an array: " + line);
    else if (line.length == 0) throw ("geoTools.deduplicateLine no points in line: " + line);
    else if (line.length < 2) throw ("geoTools.deduplicateLine line is a point: " + line);
    const dedup = [];
    let lastPosition = [null, null];
    line.forEach((position) => {
        if (isNaN(position[0]) || isNaN(position[1]) || (position[0] == null) || (position[1] == null) || (position[0] == undefined) || (position[1] == undefined)) throw ("geoTools..deduplicateLine has invalid longitude/latitude pair : " + position);
        if ((Number(position[0]) != Number(lastPosition[0])) || (Number(position[1]) != Number(lastPosition[1]))) {
            dedup.push([Number(position[0]), Number(position[1])]);
            lastPosition = position;
        }
    })
    if (dedup.length < 2) throw ("geoTools.deduplicateLine deduplicated line is a point: " + dedup);
    return dedup;
}
exports.deduplicateLine = deduplicateLine;

function removeLineKinks(line) {
    if (!Array.isArray(line)) throw ("geoTools.removeLineKinks line is not an array: " + line);
    else if (line.lengt < 2) throw ("geoTools.removeLineKinks less than two positions in array: " + line);
    const deduplicatedLine = deduplicateLine(line);
    const resultLine = [];
    let currentPos = 0;
    let deleteBuffer = [];

    deduplicatedLine.forEach(point => {

    });



    return resultLine;
}

function getBearing(fromPosition, toPosition) {
    let bearing = turf.bearing(fromPosition, toPosition);
    if (bearing < 0)
        bearing = 360 + bearing;

    return bearing;
}
exports.getBearing = getBearing;

function bearingDifference(bearing1, bearing2) {
    let lowBearing = bearing1;
    let highBearing = bearing2;
    if (lowBearing > highBearing) {
        const tempBearing = lowBearing;
        lowBearing = highBearing;
        highBearing = tempBearing;
    }
    let angleOverNorth = 360;
    if ((lowBearing < 180) && (highBearing > 180))
        angleOverNorth = (360 - highBearing) + lowBearing;

    let angleDiff = highBearing - lowBearing;

    if (angleOverNorth < angleDiff)
        angleDiff = angleOverNorth;


    return angleDiff;
}
exports.bearingDifference = bearingDifference;