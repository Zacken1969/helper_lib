const variableCheck = require("./variables");

function toDate(timestamp) {
    let result = null;
    if (!isNaN(timestamp)) result = timestamp;
    else
        try {
            result = Date.parse(timestamp);
        } catch {
            throw ("time.isoToDate called with invalid date: " + timestamp);
        }
    if (!isNaN(result)) return new Date(result);
    else throw ("time.isoToDate called with invalid date: " + timestamp);
}
exports.toDate = toDate;

function isDate(timestamp) {
    try {
        toDate(timestamp);
    } catch {
        return false;
    }
    return true;

}
exports.isDate = isDate;


function isOlderThan(timestamp, age) {
    const now = new Date();
    let ageInMs = 0;
    if (variableCheck.isObject(age)) {
        if (age?.milliseconds) ageInMs = age.milliseconds;
        if (age?.seconds) ageInMs = ageInMs + (age.seconds * 1000);
        if (age?.minutes) ageInMs = ageInMs + (age.minutes * 60 * 1000);
        if (age?.hours) ageInMs = ageInMs + (age.hours * 60 * 60 * 1000);
        if (age?.days) ageInMs = ageInMs + (age.days * 24 * 60 * 60 * 1000);
    } else if (!isNaN) ageInMs = age;
    else throw ("time.isOlderThan was called with an invalid age: " + age);

    if (isDate(timestamp)) {
        const then = toDate(timestamp);
        const dateDiff = now - then;
        if (dateDiff > ageInMs) return true;
        else return false;
    } else throw ("time.isOlderThan was called with an invalid timestamp: " + timestamp);
}
exports.isOlderThan = isOlderThan;