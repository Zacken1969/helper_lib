const chai = require('chai');
const asserttype = require('chai-asserttype');
chai.use(asserttype);
const expect = chai.expect;

const geoTools = require('../geoTools')

describe("geoTools.deduplicateLine(line)", () => {

	it("should throw error for invalid line 0", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine(0)).to.throw();
    })
    
    it("should throw error for line that is a point after deduplication [[1,1],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine([[1,1],[1,1]])).to.throw();
    })

    it("should throw error for line with invalid longitude [['x',1],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine([['x',1],[1,1]])).to.throw();
    })

    it("should throw error for line with invalid latitude [[2,null],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine([[2,null],[1,1]])).to.throw();
    })

    it("should throw error for line that is a point after deduplication [[4,5]]", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine([4,5])).to.throw();
    })

    it("should not throw error for line with latitude as a string and return value as a number [[2,'1'],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
        expect(()=> geoTools.deduplicateLine([[2,'1'],[1,1]])).not.to.throw();
        expect(geoTools.deduplicateLine([[2,'1'],[1,1]])[0][1]).not.to.be.string();
    })

    it("should not throw error for line with longitude as a string and return value as a number [['2',1],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
        expect(()=> geoTools.deduplicateLine([['2',1],[1,1]])).not.to.throw();
        expect(geoTools.deduplicateLine([['2',1],[1,1]])[0][0]).not.to.be.string();
    })
    
    it("should return correct length of line as 5 after deduplication for [[1,1],[2,2],[3,3],[3,3],[3,3],[2,2],[1,1],[1,1]]", ()=> {
        const result=geoTools.deduplicateLine([[1,1],[2,2],[3,3],[3,3],[3,3],[2,2],[1,1],[1,1]]);
        expect(result.length).to.be.equal(5);
    })

    it("should return correct length of line as 5 after deduplication for [[1,1],[2,2],[3,3],[3,3],[3,3],[2,2],[1,1],[1,1]]", ()=> {
        const result=geoTools.deduplicateLine([[1,1],[2,2],[3,3],[3,3],[3,3],[2,2],[1,1],[1,1]]);
        expect(result.length).to.be.equal(5);
    })
})


describe("geoTools.getBearing(fromPosition, toPosition)", () => {

	/*it("should throw error for invalid line 0", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine(0)).to.throw();
    })
    
    it("should throw error for line that is a point after deduplication [[1,1],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine([[1,1],[1,1]])).to.throw();
    })

    it("should throw error for line with invalid longitude [['x',1],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine([['x',1],[1,1]])).to.throw();
    })

    it("should throw error for line with invalid latitude [[2,null],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine([[2,null],[1,1]])).to.throw();
    })

    it("should throw error for line that is a point after deduplication [[4,5]]", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> geoTools.deduplicateLine([4,5])).to.throw();
    })

    it("should not throw error for line with latitude as a string and return value as a number [[2,'1'],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
        expect(()=> geoTools.deduplicateLine([[2,'1'],[1,1]])).not.to.throw();
        expect(geoTools.deduplicateLine([[2,'1'],[1,1]])[0][1]).not.to.be.string();
    })

    it("should not throw error for line with longitude as a string and return value as a number [['2',1],[1,1]]", ()=> {
        //assert.throws(time.toDate("x"))
        expect(()=> geoTools.deduplicateLine([['2',1],[1,1]])).not.to.throw();
        expect(geoTools.deduplicateLine([['2',1],[1,1]])[0][0]).not.to.be.string();
    })*/
    
    it("should return 90 degrees [0,0],[1,0]", ()=> {
        const result=geoTools.getBearing([0,0],[1,0]);
        expect(result).to.be.equal(90);
    })
    it("should return 270 degrees [0,0],[-1,0]", ()=> {
        const result=geoTools.getBearing([0,0],[-1,0]);
        expect(result).to.be.equal(270);
    })
    it("should return 0 degrees [0,0],[0,1]", ()=> {
        const result=geoTools.getBearing([0,0],[0,1]);
        expect(result).to.be.equal(0);
    })
    it("should return 180 degrees [0,0],[0,-1]", ()=> {
        const result=geoTools.getBearing([0,0],[0,-1]);
        expect(result).to.be.equal(180);
    })
})


describe("geoTools.bearingDifference(bearing1, bearing2)", () => {
    
    it("should return 20 degrees (10,350)", ()=> {
        const result=geoTools.bearingDifference(10, 350);
        expect(result).to.be.equal(20);
    })

    it("should return 180 degrees (0,180)", ()=> {
        const result=geoTools.bearingDifference(0, 180);
        expect(result).to.be.equal(180);
    })

    it("should return 90 degrees (45,135)", ()=> {
        const result=geoTools.bearingDifference(45,135);
        expect(result).to.be.equal(90);
    })
    it("should return 90 degrees (280,10)", ()=> {
        const result=geoTools.bearingDifference(280,10);
        expect(result).to.be.equal(90);
    })
    it("should return 0 degrees (359,359)", ()=> {
        const result=geoTools.bearingDifference(359,359);
        expect(result).to.be.equal(0);
    })
    it("should return 178 degrees (271,89)", ()=> {
        const result=geoTools.bearingDifference(271,89);
        expect(result).to.be.equal(178);
    })
    it("should return 178 degrees (269,91)", ()=> {
        const result=geoTools.bearingDifference(269,91);
        expect(result).to.be.equal(178);
    })
    it("should return 3 degrees (358,1)", ()=> {
        const result=geoTools.bearingDifference(358,1);
        expect(result).to.be.equal(3);
    })
})