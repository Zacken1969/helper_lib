const chai = require('chai');
const asserttype = require('chai-asserttype');
chai.use(asserttype);
const expect = chai.expect;

const time = require('../time')

describe("time.toDate(timestamp)", () => {

	it("should return throw error for invalid date 'x'", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> time.toDate("x")).to.throw();
    })
    
    it("should return throw error for invalid date '2020-25-11'", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> time.toDate("2020-25-11")).to.throw();
    })

    it("should return throw error for invalid date '2020-11-11T'", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> time.toDate("2020-11-11T")).to.throw();
    })

    it("should return throw error for invalid date '2020-01-03T13'", ()=> {
        //assert.throws(time.toDate("x"))
		expect(()=> time.toDate("2020-01-03T13")).to.throw();
    })
    
    it("should return correct date for unix timstamp 1605258000021", ()=> {
        //assert.throws(time.toDate("x"))
        const testTS=Date.parse("2020-11-13T09:00:00Z");
        const result=time.toDate(1605258000000);
        expect(result).to.be.date();
        expect(result.valueOf()).to.be.equal(testTS.valueOf())
      
    })
    it("should return correct date for ISO timstamp '2020-01-03'", ()=> {
        //assert.throws(time.toDate("x"))
        const dateString="2020-01-03";
        const testTS=new Date(dateString);
        const result=time.toDate(dateString);
        expect(result).to.be.date();
		expect(result.valueOf()).be.equal(testTS.valueOf())
    })
    
    it("should return correct date for ISO timstamp '2020-01-03T13:02'", ()=> {
        const dateString="2020-01-03T13:02";
        const testTS=new Date(dateString);
        const result=time.toDate(dateString);
        expect(result).to.be.date();
		expect(result.valueOf()).to.be.equal(testTS.valueOf())
	})
    it("should return correct date for ISO timstamp '2020-01-03T13:02:00'", ()=> {
        const dateString="2020-01-03T13:02:00";
        const testTS=new Date(dateString);
        const result=time.toDate(dateString);
        expect(result).to.be.date();
		expect(result.valueOf()).to.be.equal(testTS.valueOf())
    })
    it("should return date for ISO timstamp '2020-01-03T13:02:00'", ()=> {
        const dateString="2020-01-03T13:02:00";
        const testTS=new Date(dateString);
        const result=time.toDate(dateString);
        expect(result).to.be.date();
		expect(result.valueOf()).to.be.equal(testTS.valueOf())
    })
    it("should return date for ISO timstamp '2020-01-03T13:02:00Z'", ()=> {
        const dateString="2020-01-03T13:02:00Z";
        const testTS=new Date(dateString);
        const result=time.toDate(dateString);
        expect(result).to.be.date();
		expect(result.valueOf()).to.be.equal(testTS.valueOf())
    })
    it("should return date for ISO timstamp '2020-01-03T13:02:00+01:00'", ()=> {
        const dateString="2020-01-03T13:02:00+01:00";
        const testTS=new Date(dateString);
        const result=time.toDate(dateString);
        expect(result).to.be.date();
		expect(result.valueOf()).to.be.equal(testTS.valueOf())
	})

})

describe("time.isDate(timestamp)", () => {

	it("should return false on invalid date 'x'", ()=> {
        //assert.throws(time.toDate("x"))
		expect(time.isDate("x")).to.be.false;
    })
    
    it("should return false for invalid date '2020-25-11'", ()=> {
        //assert.throws(time.toDate("x"))
		expect(time.isDate("2020-25-11")).to.be.false;
    })

    it("should return false for invalid date '2020-11-11T'", ()=> {
        //assert.throws(time.toDate("x"))
		expect(time.isDate("2020-11-11T")).to.be.false;
    })

    it("should return false for invalid date '2020-01-03T13'", ()=> {
        //assert.throws(time.toDate("x"))
        expect(time.isDate("2020-11-11T13")).to.be.false;
    })
    
    it("should return true for unix timstamp 1605258000021", ()=> {
        //assert.throws(time.toDate("x"))
        expect(time.isDate(1605258000021)).to.be.true;
    })
    it("should return true for ISO timstamp '2020-01-03'", ()=> {
        //assert.throws(time.toDate("x"))
        expect(time.isDate("2020-01-03")).to.be.true;
    })
    
    it("should return true for ISO timstamp '2020-01-03T13:02'", ()=> {
        const dateString="2020-01-03T13:02";
        expect(time.isDate(dateString)).to.be.true;
	})
    it("should return true for ISO timstamp '2020-01-03T13:02:00'", ()=> {
        const dateString="2020-01-03T13:02:00";
        expect(time.isDate(dateString)).to.be.true;
    })
    it("should return true for ISO timstamp '2020-01-03T13:02:00'", ()=> {
        const dateString="2020-01-03T13:02:00";
        expect(time.isDate(dateString)).to.be.true;
    })
    it("should return true for ISO timstamp '2020-01-03T13:02:00Z'", ()=> {
        const dateString="2020-01-03T13:02:00Z";
        expect(time.isDate(dateString)).to.be.true;
    })
    it("should return true for ISO timstamp '2020-01-03T13:02:00+01:00'", ()=> {
        const dateString="2020-01-03T13:02:00+01:00";
        expect(time.isDate(dateString)).to.be.true;
	})

	
	
})