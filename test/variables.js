const chai = require('chai')
const expect = chai.expect

const variables = require('../variables')

describe("variables.isObject(object)", () => {

	it("should return true for an empty object", ()=> {
		expect(variables.isObject({})).to.be.true
	})

	it("should return true for an object", () => {
		expect(variables.isObject({foo:"BAR"})).to.be.true
	})

	it("should return false for a number", () => {
		expect(variables.isObject(123)).to.be.false
    })
    it("should return false for the number 0", () => {
		expect(variables.isObject(0)).to.be.false
    })
    it("should return false for null", () => {
		expect(variables.isObject(null)).to.be.false
    })
    it("should return false for undefined", () => {
		expect(variables.isObject(undefined)).to.be.false
    })
    it("should return false for an array", () => {
		expect(variables.isObject([1,2,3,4])).to.be.false
	})
})

describe("variables.isNullish(variable)", () => {

	it("should return false for an object", ()=> {
		expect(variables.isNullish({foo:"bar"})).to.be.false
	})

	it("should return false for a string", () => {
		expect(variables.isNullish("Hello")).to.be.false
	})

	it("should return false for a number", () => {
		expect(variables.isNullish(123)).to.be.false
    })
    it("should return true for null", () => {
		expect(variables.isNullish(null)).to.be.true
    })
    it("should return true for undefined", () => {
		expect(variables.isNullish(undefined)).to.be.true
    })
    it("should return false for an array", () => {
		expect(variables.isNullish([1,2,3,4])).to.be.false
	})
})