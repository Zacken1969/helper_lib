function isObject(object){
    return typeof object === 'object' && object !== null && !Array.isArray(object);
}

exports.isObject = isObject;

function isNullish(variable){
    return variable === null || variable === undefined;
}

exports.isNullish = isNullish;